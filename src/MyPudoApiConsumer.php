<?php

declare(strict_types=1);

namespace Drupal\commerce_dpd_fr;

use GuzzleHttp\Client;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\Exception\ClientException;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * The Guest Suite API consumer.
 */
class MyPudoApiConsumer {

  use StringTranslationTrait;

  private const API_URL = 'http://mypudo.pickup-services.com/mypudo/mypudo.asmx/';

  /**
   * The Commerce DPD France logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Contructs a new MyPudoApiConsumer instance.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \GuzzleHttp\Client $http_client
   *   The http client.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory, Client $http_client) {
    $this->logger = $logger_factory->get('commerce_dpd_fr');
    $this->httpClient = $http_client;
  }

  /**
   * Gets endpoint data.
   *
   * @param string $end_point
   *   The endpoint (without extension).
   * @param string $query
   *   Query params.
   *
   * @return array|null
   *   The requested data on success,
   *   NULL on access denied,
   *   an empty array otherwise.
   */
  protected function getEndpointData(string $end_point, $query = []): ?array {
    try {
      $response = $this->httpClient->request(
        'GET',
        self::API_URL . $end_point,
        ['query' => $query]
      );
      $xml = new \SimpleXMLElement($response->getBody()->getContents());
      return Json::decode(Json::encode($xml));
    }
    catch (ClientException $e) {
      $this->logger->error($e->getMessage());
    }

    return NULL;
  }

  /**
   * Gets the details of a pickup point from its id.
   *
   * @param string $api_carrier
   *   The API carrier.
   * @param string $api_key
   *   The API key.
   * @param string $pickup_id
   *   The pickup point id.
   *
   * @return array|null
   *   The requested data on success,
   *   NULL on access denied,
   *   an empty array otherwise.
   */
  public function getPickupPointDetails(string $api_carrier, string $api_key, string $pickup_id): ?array {
    $data = $this->getEndpointData('GetPudoDetails', [
      'carrier' => $api_carrier,
      'key' => $api_key,
      'pudo_id' => $pickup_id,
    ]);

    if (isset($data['PUDO_ITEMS']['PUDO_ITEM'])) {
      return $this->formatPickupPointList([$data['PUDO_ITEMS']['PUDO_ITEM']]);
    }

    return NULL;
  }

  /**
   * Gets the pickup point list from customer address.
   *
   * @param string $api_carrier
   *   The API carrier.
   * @param string $api_key
   *   The API key.
   * @param array $data
   *   The data.
   *
   * @return array|null
   *   The requested data on success,
   *   NULL on access denied,
   *   an empty array otherwise.
   */
  public function getPickupPointList(string $api_carrier, string $api_key, array $data): ?array {
    $data = $this->getEndpointData('GetPudoList', [
      'carrier' => $api_carrier,
      'key' => $api_key,
      'address' => $data['address_line1'],
      'zipCode' => $data['postal_code'],
      'city' => $data['locality'],
      'countrycode' => $data['country_code'],
      'requestID' => $data['order_id'],
      'date_from' => date('d/m/Y'),
      'max_pudo_number' => '',
      'max_distance_search' => '',
      'weight' => '',
      'category' => '',
      'holiday_tolerant' => '',
    ]);

    if (isset($data['PUDO_ITEMS']['PUDO_ITEM'])) {
      return $this->formatPickupPointList($data['PUDO_ITEMS']['PUDO_ITEM']);
    }

    return NULL;
  }

  /**
   * Formats pickup point list response from API.
   *
   * @param array $pickup_point_list
   *   The non formatted pickup point list.
   *
   * @return array
   *   The formatted pickup point list.
   */
  protected function formatPickupPointList(array $pickup_point_list): array {
    $formatted_data = [];
    $days = [
      1 => $this->t('Monday'),
      2 => $this->t('Tuesday'),
      3 => $this->t('Wednesday'),
      4 => $this->t('Thursday'),
      5 => $this->t('Friday'),
      6 => $this->t('Saturday'),
      7 => $this->t('Sunday'),
    ];

    foreach ($pickup_point_list as $item) {
      $formatted_item = &$formatted_data[$item['PUDO_ID']];
      $formatted_item['name'] = $item['NAME'] ?? NULL;
      $formatted_item['address1'] = $item['ADDRESS1'] ?? NULL;
      $formatted_item['address2'] = $item['ADDRESS2'] ?? NULL;
      $formatted_item['address3'] = $item['ADDRESS3'] ?? NULL;
      $formatted_item['local_hint'] = $item['LOCAL_HINT'] ?? NULL;
      $formatted_item['zipcode'] = $item['ZIPCODE'] ?? NULL;
      $formatted_item['city'] = $item['CITY'] ?? NULL;
      $formatted_item['lat'] = isset($item['LATITUDE']) ? (float) strtr($item['LATITUDE'], ',', '.') : NULL;
      $formatted_item['lng'] = isset($item['LONGITUDE']) ? (float) strtr($item['LONGITUDE'], ',', '.') : NULL;
      $formatted_item['distance'] = isset($item['DISTANCE']) ? (int) $item['DISTANCE']: NULL;
      $formatted_item['map_url'] = $item['MAP_URL'] ?? NULL;
      $formatted_item['available'] = $item['AVAILABLE'] ?? NULL;

      $opening_days = [];
      if (isset($item['OPENING_HOURS_ITEMS']['OPENING_HOURS_ITEM'])) {
        $opening_hours = $item['OPENING_HOURS_ITEMS']['OPENING_HOURS_ITEM'];
        if (!empty($opening_hours)) {
          foreach ($days as $key => $day) {
            $string_day = $day->__toString();
            $opening_days[$string_day] = [];
            foreach ($opening_hours as $oh_item) {
              if ($oh_item['DAY_ID'] == $key) {
                $opening_days[$day->__toString()][] = $oh_item['START_TM'] . ' - ' . $oh_item['END_TM'];
              }
            }
            if (empty($opening_days[$string_day])) {
              $opening_days[$string_day][] = $this->t('Closed');
            }
          }
        }
      }
      $formatted_item['opening_hours'] = $opening_days;

      $holiday_days = [];
      if (isset($item['HOLIDAY_ITEMS']['HOLIDAY_ITEM'])) {
        foreach ($item['HOLIDAY_ITEMS'] as $holiday_item) {
          $holiday_days['closing_period'][] = $holiday_item['START_DTM'] . ' - ' . $holiday_item['END_DTM'];
        }
      }
      $formatted_item['closing_period'] = $holiday_days;

      unset($formatted_item);
    }

    return $formatted_data;
  }

}
