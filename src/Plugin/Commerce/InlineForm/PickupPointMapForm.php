<?php

namespace Drupal\commerce_dpd_fr\Plugin\Commerce\InlineForm;

use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an inline form for selecting a pickup point on a map.
 *
 * @CommerceInlineForm(
 *   id = "commerce_dpd_fr_pickup_point_map_form",
 *   label = @Translation("Commerce DPD France pickup point map form"),
 * )
 */
class PickupPointMapForm extends PickupPointForm {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The admin context.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * Constructs a new CustomerProfile object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The commerce shipment storage interface.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   A request stack symfony instance.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The admin context.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler,
    RequestStack $request_stack,
    AdminContext $admin_context
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager);

    $this->moduleHandler = $module_handler;
    $this->request = $request_stack->getCurrentRequest();
    $this->adminContext = $admin_context;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('request_stack'),
      $container->get('router.admin_context'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildInlineForm(array $inline_form, FormStateInterface $form_state) {
    $inline_form = parent::buildInlineForm($inline_form, $form_state);
    if (empty($inline_form['pickup_point_list']['#value'])) {
      return $inline_form;
    };

    // Saves the shipping method id in the order to get
    // the Google API Key in the library info alter.
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->entityTypeManager->getStorage('commerce_order')->load($this->configuration['order_id']);
    $order->setData('commerce_dpd_fr_shipping_method_id', $this->configuration['shipping_method_id']);
    $order->save();

    if ($this->adminContext->isAdminRoute()) {
      $radio_path = 'pickup_point_id';
    }
    else {
      $radio_parents = array_merge($inline_form['#parents'], ['pickup_point_id']);
      $radio_path = array_shift($radio_parents);
      $radio_path .= '[' . implode('][', $radio_parents) . ']';
    }

    $customer_address = $inline_form['address']['#default_value'];
    /** @var \Drupal\commerce_shipping\ShippingMethodInterface $shipping_method */
    $shipping_method = $this->entityTypeManager
      ->getStorage('commerce_shipping_method')
      ->load($this->configuration['shipping_method_id']);
    $customer_location = $shipping_method->getPlugin()->getCustomerAddressLocation($customer_address);

    $host = $this->request->getSchemeAndHttpHost();
    $module_path = $this->moduleHandler->getModule('commerce_dpd_fr')->getPath();

    $inline_form['#attached']['library'][] = 'commerce_dpd_fr/commerce_dpd_fr_pickup_point_map';
    $inline_form['#attached']['library'][] = 'commerce_dpd_fr/google-map-api';
    $inline_form['#attached']['drupalSettings']['commerce_dpd_fr_pickup_point_map'] = [
      'radio_path' => $radio_path,
      'customer_location' => $customer_location,
      'module_path' => $host . '/' . $module_path,
    ];

    $inline_form['#attached']['drupalSettings']['commerce_dpd_fr_pickup_point_map']['pickup_point_list'][] =
      $inline_form['pickup_point_list']['#value'];

    $inline_form['map'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'js-commerce-dpd-fr-pickup-point-map',
      ],
    ];

    return $inline_form;
  }

}
