<?php

namespace Drupal\commerce_dpd_fr\Plugin\Commerce\InlineForm;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use CommerceGuys\Addressing\AddressFormat\AddressField;
use CommerceGuys\Addressing\AddressFormat\FieldOverride;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce\Plugin\Commerce\InlineForm\InlineFormBase;

/**
 * Provides an inline form for selecting a pickup point on a list.
 *
 * @CommerceInlineForm(
 *   id = "commerce_dpd_fr_pickup_point_form",
 *   label = @Translation("Commerce DPD France pickup point form"),
 * )
 */
class PickupPointForm extends InlineFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new CustomerProfile object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The commerce shipment storage interface.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'shipping_profile_address' => [],
      'order_id' => '',
      'shipment_id' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildInlineForm(array $inline_form, FormStateInterface $form_state) {
    $inline_form = parent::buildInlineForm($inline_form, $form_state);

    $customer_address = $this->getCustomerAddress($inline_form, $form_state);
    $inline_form['address'] = [
      '#type' => 'address',
      '#default_value' => $customer_address,
      // The needed address fields need to be optional otherwise they trigger
      // the autorecalcute shipping method from commerce_shipping.
      '#field_overrides' => [
        AddressField::ADMINISTRATIVE_AREA => FieldOverride::HIDDEN,
        AddressField::LOCALITY => FieldOverride::OPTIONAL,
        AddressField::POSTAL_CODE => FieldOverride::OPTIONAL,
        AddressField::SORTING_CODE => FieldOverride::HIDDEN,
        AddressField::DEPENDENT_LOCALITY => FieldOverride::HIDDEN,
        AddressField::ADDRESS_LINE1 => FieldOverride::OPTIONAL,
        AddressField::ADDRESS_LINE2 => FieldOverride::HIDDEN,
        AddressField::ORGANIZATION => FieldOverride::HIDDEN,
        AddressField::GIVEN_NAME => FieldOverride::HIDDEN,
        AddressField::ADDITIONAL_NAME => FieldOverride::HIDDEN,
        AddressField::FAMILY_NAME => FieldOverride::HIDDEN,
      ],
      // This module is only available for France for now.
      '#available_countries' => ['FR'],
    ];

    $inline_form['refresh_pickup_point_list'] = [
      '#type' => 'submit',
      '#value' => $this->t('Refresh pickup point list'),
      '#submit' => [
        [$this, 'refreshPickupPointList'],
      ],
      '#limit_validation_errors' => [
        $inline_form['#parents'],
      ],
      '#ajax' => [
        'callback' => [$this, 'ajaxRefreshForm'],
        'element' => $inline_form['#parents'],
      ],
    ];

    $request_data = $customer_address + [
      'order_id' => $this->configuration['order_id'],
    ];

    /** @var \Drupal\commerce_shipping\ShippingMethodInterface $shipping_method */
    $shipping_method = $this->entityTypeManager
      ->getStorage('commerce_shipping_method')
      ->load($this->configuration['shipping_method_id']);
    $pickup_point_list = $shipping_method->getPlugin()->getPickupPointList($request_data);

    // Set the pickup point list in the inline form to get it in child class.
    $inline_form['pickup_point_list'] = [
      '#type' => 'value',
      '#value' => $pickup_point_list,
    ];

    if (!empty($pickup_point_list)) {
      $radio_options = [];
      foreach ($pickup_point_list as $id => $pickup_point) {
        $radio_options[$id] = $pickup_point['name'];
      }

      if ($this->configuration['shipment_id']) {
        /** @var \Drupal\commerce_shipping\ShipmentInterface $shipment */
        $shipment = $this->entityTypeManager->getStorage('commerce_shipment')->load($this->configuration['shipment_id']);
      }
      if (isset($shipment) && array_key_exists($shipment->get('commerce_dpd_fr_pickup_point_id')->value, $radio_options)
      ) {
        $default_value = $shipment->get('commerce_dpd_fr_pickup_point_id')->value;
      }
      else {
        $default_value = array_key_first($radio_options);
      }

      $inline_form['pickup_point_id'] = [
        '#type' => 'radios',
        '#title' => $this->t('Pickup point'),
        '#options' => $radio_options,
        '#default_value' => $default_value,
        '#required' => TRUE,
      ];

      foreach ($pickup_point_list as $id => $pickup_point) {
        $inline_form['pickup_point_id'][$id] = [
          '#description' => $pickup_point['address1'] . ', ' . $pickup_point['zipcode'] . ' ' . $pickup_point['city'],
        ];
      }
    }
    else {
      $inline_form['pickup_point_id'] = [
        '#type' => 'item',
        '#markup' => $this->t('No pickup point available for this address. Please provide another address.'),
      ];
    }

    return $inline_form;
  }

  /**
   * Gets the customer address related to pickup points.
   *
   * @param array $inline_form
   *   The inline form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The address.
   */
  protected function getCustomerAddress(array $inline_form, FormStateInterface $form_state): array {
    if ($this->configuration['shipment_id']) {
      /** @var \Drupal\commerce_shipping\ShipmentInterface $shipment */
      $shipment = $this->entityTypeManager
        ->getStorage('commerce_shipment')
        ->load($this->configuration['shipment_id']);
    }

    $parents = $form_state->getValue($inline_form['#parents']);
    if (isset($shipment) && $shipment->getData('customer_pickup_point_address')) {
      // First checks if an address was previously stored in the shipment,
      // returns it.
      return $shipment->getData('customer_pickup_point_address');
    }
    elseif (isset($parents['address'])) {
      // Else checks if the form state contains the previous address,
      // returns it.
      return $parents['address'];
    }
    elseif (!empty($this->configuration['shipping_profile_address'])) {
      // Else checks if the shipping profile address was passed in the
      // form configuration and returns it.
      return $this->configuration['shipping_profile_address'];
    }

    // Otherwise, returns an empty array.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function validateInlineForm(array &$inline_form, FormStateInterface $form_state) {
    parent::validateInlineForm($inline_form, $form_state);

    // We need to validate the form only on main form submit in order to refresh
    // the list when the address was changed even if no pickup is selected.
    $triggering_element = $form_state->getTriggeringElement();
    $button_type = $triggering_element['#button_type'] ?? NULL;
    if ($button_type != 'primary') {
      return;
    }

    $pickup_point_id_parents = array_merge($inline_form['#parents'], ['pickup_point_id']);
    $pickup_point_id_path = implode('][', $pickup_point_id_parents);
    if (!$form_state->getValue($pickup_point_id_parents)) {
      $form_state->setErrorByName($pickup_point_id_path, $this->t('Please select a pickup point.'));
    }
  }

  /**
   * Submit handler to refresh pickup point list.
   *
   *
   */
  public function refreshPickupPointList(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $parents = array_slice($triggering_element['#parents'], 0, -1);
    $values = $form_state->getValues();

    // Save the address in the shipment data.
    /** @var \Drupal\commerce_shipping\ShipmentInterface $shipment */
    $shipment = $shipment = $this->entityTypeManager
      ->getStorage('commerce_shipment')
      ->load($this->configuration['shipment_id']);
    $address = NestedArray::getValue($values, array_merge($parents, ['address']));
    $shipment->setData('customer_pickup_point_address', $address);
    $shipment->save();

    // Clear the pickup point id radios.
    $user_input = &$form_state->getUserInput();
    NestedArray::setValue($user_input, array_merge($parents, ['pickup_point_id']), NULL);

    $form_state->setRebuild();
  }

}
