<?php

declare(strict_types = 1);

namespace Drupal\commerce_dpd_fr\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_dpd_fr\MyPudoApiConsumer;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_dpd_fr\GeocoderApiConsumer;
use Drupal\state_machine\WorkflowManagerInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;

/**
 * Provides the Rate per weight shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "dpd_fr",
 *   label = @Translation("DPD France"),
 *   services = {
 *     "PICKUP" = @Translation("DPD France pickup point"),
 *   }
 * )
 */
class DpdFrance extends ShippingMethodBase implements DpdFranceInterface {

  /**
   * The My Pudo API consumer.
   *
   * @var \Drupal\commerce_dpd_fr\MyPudoApiConsumer
   */
  protected $myPudoApiConsumer;

  /**
   * The Google map geocoder API consumer.
   *
   * @var \Drupal\commerce_dpd_fr\GeocoderApiConsumer
   */
  protected $geocoderApiConsumer;

  /**
   * Constructs a new FlatRate object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   * @param \Drupal\commerce_dpd_fr\MyPudoApiConsumer $my_pudo_api_consumer
   *   The My Pudo API consumer.
   * @param \Drupal\commerce_dpd_fr\GeocoderApiConsumer $geocoder_api_consumer
   *   The Google Map Geocoder API consumer.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    PackageTypeManagerInterface $package_type_manager,
    WorkflowManagerInterface $workflow_manager,
    MyPudoApiConsumer $my_pudo_api_consumer,
    GeocoderApiConsumer $geocoder_api_consumer
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager);

    $this->myPudoApiConsumer = $my_pudo_api_consumer;
    $this->geocoderApiConsumer = $geocoder_api_consumer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow'),
      $container->get('commerce_dpd_fr.my_pudo_api_consumer'),
      $container->get('commerce_dpd_fr.google_map_geocoder_api_consumer'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'rate_label' => '',
      'rate_description' => '',
      'mypudo_api_information' => [
        'carrier' => '',
        'key' => '',
      ],
      'google_api_information' => [
        'key' => '',
      ],
      'rate' => [
        'mode' => 'flat',
        'options' => [
          'flat' => [
            'rate_amount' => NULL,
          ],
        ],
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Select all services by default.
    if (empty($this->configuration['services'])) {
      $service_ids = array_keys($this->services);
      $this->configuration['services'] = array_combine($service_ids, $service_ids);
    }

    $form['rate_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rate label'),
      '#description' => $this->t('Shown to customers when selecting the rate.'),
      '#default_value' => $this->configuration['rate_label'],
      '#required' => TRUE,
    ];

    $form['rate_description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rate description'),
      '#description' => $this->t('Provides additional details about the rate to the customer.'),
      '#default_value' => $this->configuration['rate_description'],
    ];

    $form['mypudo_api_information'] = [
      '#type' => 'details',
      '#title' => $this->t('My Pudo API information'),
      '#open' => TRUE,
    ];
    $form['mypudo_api_information']['carrier'] = [
      '#type' => 'textfield',
      '#title' => $this->t('My Pudo API carrier'),
      '#description' => $this->t('Obtain the My Pudo API carrier from your DPD France adviser.'),
      '#default_value' => $this->configuration['mypudo_api_information']['carrier'],
      '#required' => TRUE,
    ];
    $form['mypudo_api_information']['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('My Pudo API key'),
      '#description' => $this->t('Obtain the My Pudo API key from your DPD France adviser.'),
      '#default_value' => $this->configuration['mypudo_api_information']['key'],
      '#required' => TRUE,
    ];

    $form['google_api_information'] = [
      '#type' => 'details',
      '#title' => $this->t('Google Maps API information'),
      '#description' => $this->t('Required to display the DPD France pickup map form.'),
      '#open' => TRUE,
    ];
    $form['google_api_information']['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Maps API key'),
      '#description' => $this->t('Obtain a Google Maps Javascript API key at <a href="@link">@link</a>', [
        '@link' => 'https://developers.google.com/maps/documentation/javascript/get-api-key',
      ]),
      '#default_value' => $this->configuration['google_api_information']['key'],
      '#required' => TRUE,
      '#size' => 80,
    ];

    $form['rate'] = [
      '#type' => 'details',
      '#title' => $this->t('Rate'),
      '#open' => TRUE,
    ];
    $form['rate']['mode'] = [
      '#type' => 'radios',
      '#default_value' => $this->configuration['rate']['mode'],
      '#options' => [
        'flat' => $this->t('Flat rate'),
      ],
      '#title' => $this->t('Rate'),
      '#ajax' => [
        'wrapper' => 'rate_options',
        'callback' => [$this, 'ajaxRefresh'],
      ],
    ];
    $form['rate']['options'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'rate_options',
      ],
    ];

    $flat_type = $form_state->getValue(
      array_merge($form['#parents'], ['rate', 'mode'])
    );

    switch ($flat_type) {
      default:
      case 'flat':
        $amount = $this->configuration['rate']['options']['flat']['rate_amount'];
        // A bug in the plugin_select form element causes $amount to be incomplete.
        if (isset($amount) && !isset($amount['number'], $amount['currency_code'])) {
          $amount = NULL;
        }

        $form['rate']['options']['flat']['rate_amount'] = [
          '#type' => 'commerce_price',
          '#title' => $this->t('Rate amount'),
          '#default_value' => $amount,
          '#required' => TRUE,
        ];
        break;
    }

    return $form;
  }

   /**
   * @inheritDoc
   */
  public function ajaxRefresh(array &$form, FormStateInterface $form_state): array {
    return $form['plugin']['widget'][0]['target_plugin_configuration']['form']['rate']['options'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['rate_label'] = $values['rate_label'];
      $this->configuration['rate_description'] = $values['rate_description'];
      $this->configuration['mypudo_api_information']['carrier'] = $values['mypudo_api_information']['carrier'];
      $this->configuration['mypudo_api_information']['key'] = $values['mypudo_api_information']['key'];
      $this->configuration['google_api_information']['key'] = $values['google_api_information']['key'];
      $this->configuration['rate']['mode'] = $values['rate']['mode'];
      $this->configuration['rate']['options']['flat']['rate_amount'] = $values['rate']['options']['flat']['rate_amount'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRates(ShipmentInterface $shipment) {
    $rates = [];

    switch ($this->configuration['rate']['mode']) {
      case 'flat':
        foreach ($this->configuration['services'] as $service_id) {
          $rates[] = new ShippingRate([
            'shipping_method_id' => $this->parentEntity->id(),
            'service' => $this->services[$service_id],
            'amount' => Price::fromArray($this->configuration['rate']['options']['flat']['rate_amount']),
            'description' => $this->configuration['rate_description'],
          ]);
        }
        break;

      default:
        break;
    }

    return $rates;

  }

  /**
   * {@inheritdoc}
   */
  public function applies(ShipmentInterface $shipment) {
    $shipping_address = $shipment->getShippingProfile()->get('address')->first()?->getValue();

    return $shipping_address ? $shipping_address['country_code'] === 'FR' : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getGoogleApiKey(): string {
    return $this->configuration['google_api_information']['key'];
  }

  /**
   * {@inheritdoc}
   */
  public function getPickupPointList(array $data): ?array {
    return $this->myPudoApiConsumer->getPickupPointList(
      $this->configuration['mypudo_api_information']['carrier'],
      $this->configuration['mypudo_api_information']['key'],
      $data
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getPickupPointDetails(string $pickup_id): ?array {
    return $this->myPudoApiConsumer->getPickupPointDetails(
      $this->configuration['mypudo_api_information']['carrier'],
      $this->configuration['mypudo_api_information']['key'],
      $pickup_id
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCustomerAddressLocation(array $address): ?array {
    return $this->geocoderApiConsumer->getLocation(
      $this->configuration['google_api_information']['key'],
      $address
    );
  }

}
