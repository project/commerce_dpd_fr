<?php

declare(strict_types = 1);

namespace Drupal\commerce_dpd_fr\Plugin\Commerce\ShippingMethod;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the DPD France shipping method interface.
 */
interface DpdFranceInterface {

  /**
   * Refreshes the rate options on rate mode selection.
   *
   * @param array $form
   *   The form buildable array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The buildable array of the rate options form part.
   */
  public function ajaxRefresh(array &$form, FormStateInterface $form_state): array;

  /**
   * Gets the shipping method Google API key.
   *
   * @return string
   *   The Google API key.
   */
  public function getGoogleApiKey(): string;

  /**
   * Gets the pickup point list from customer address.
   *
   * @param array $data
   *   The data.
   *
   * @return array|null
   *   The pickup point list.
   */
  public function getPickupPointList(array $data): ?array;

  /**
   * Gets the details of a pickup point from pickup id.
   *
   * @param string $pickup_id
   *   The pickup point id.
   *
   * @return array|null
   *   The pickup point details.
   */
  public function getPickupPointDetails(string $pickup_id): ?array;

  /**
   * Gets the customer address location.
   *
   * @param array $address
   *   The customer address.
   *
   * @return array|null
   *   The customer address location.
   */
  public function getCustomerAddressLocation(array $address): ?array;

}
