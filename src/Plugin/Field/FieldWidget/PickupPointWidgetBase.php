<?php

declare(strict_types=1);

namespace Drupal\commerce_dpd_fr\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\commerce\InlineFormManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\ShipmentManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base functionality for Commerce DPD France pickup point widgets.
 */
class PickupPointWidgetBase extends WidgetBase {

  /**
   * The shipment manager.
   *
   * @var \Drupal\commerce_shipping\ShipmentManagerInterface
   */
  protected $shipmentManager;

  /**
   * The inline form manager.
   *
   * @var \Drupal\commerce\InlineFormManager
   */
  protected $inlineFormManager;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, InlineFormManager $inline_form_manager, ShipmentManagerInterface $shipment_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->inlineFormManager = $inline_form_manager;
    $this->shipmentManager = $shipment_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('plugin.manager.commerce_inline_form'),
      $container->get('commerce_shipping.shipment_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $entity_type = $field_definition->getTargetEntityTypeId();
    $field_name = $field_definition->getName();
    return $entity_type === 'commerce_shipment' && $field_name === 'commerce_dpd_fr_pickup_point_id';
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $value = reset($values);
    $pickup_point_id = NestedArray::getValue($form_state->getValues(), array_merge($value['array_parents'], ['pickup_point_id']));

    return $pickup_point_id;
  }

  /**
   * Gets the default shipping rate.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   Theshipment.
   *
   * @return \Drupal\commerce_shipping\ShippingRate|null
   *   The default shipping rate, or NULL if none found.
   */
  protected function getDefaultRate(array $form, FormStateInterface $form_state, ShipmentInterface $shipment): ?ShippingRate {
    $parents = array_merge($form['#parents'], ['shipping_method', 0]);
    $rates_key = implode('_', $parents);

    // Store the calculated rates in form state, so we don't have to fetch those
    // on each ajax refresh.
    if (!$form_state->has($rates_key) || $form_state->get('recalculate_shipping')) {
      $form_state->set($rates_key, $this->shipmentManager->calculateRates($shipment));
    }
    $rates = $form_state->get($rates_key);
    if (!$rates) {
      return NULL;
    }
    $default_rate = $this->shipmentManager->selectDefaultRate($shipment, $rates);

    return $default_rate;
  }

  /**
   * Constructs a pickup point form element.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param string $inline_form_id
   *   The inline form ID.
   * @param array $inline_form_configuration
   *   The inline form configuration.
   *
   * @return array
   *   The pickup form element.
   */
  protected function constructPickupPointFormElement(array $form, FormStateInterface $form_state, string $inline_form_id, array $inline_form_configuration): array {
    $element = [];

    $element['#type'] = 'fieldset';
    $element['#title'] = $this->t('Pickup point selection');
    $element['#required'] = TRUE;

    $inline_form = $this->inlineFormManager->createInstance($inline_form_id, $inline_form_configuration);

    $inline_form_element = [
      '#parents' => $form['#parents'],
      '#inline_form' => $inline_form,
    ];

    $element['pickup_point_id'] = $inline_form->buildInlineForm($inline_form_element, $form_state);

    return $element;
  }

}
