<?php

declare(strict_types=1);

namespace Drupal\commerce_dpd_fr\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'commerce_dpd_fr_pickup_point_form' widget.
 *
 * @FieldWidget(
 *   id = "commerce_dpd_fr_pickup_point_form",
 *   label = @Translation("Commerce DPD France pickup point form"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class PickupPointFormWidget extends PickupPointWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
    $shipment = $items->getEntity();
    $default_rate = $this->getDefaultRate($form, $form_state, $shipment);
    if (!$default_rate || (isset($default_rate) && $default_rate->getService()->getId() !== 'PICKUP')) {
      return $element;
    }

    $shipping_profile = $shipment->getShippingProfile();
    $element = $element + $this->constructPickupPointFormElement(
      $form,
      $form_state,
      'commerce_dpd_fr_pickup_point_form',
      [
        'shipping_profile_address' => $shipping_profile->hasField('address') ? $shipping_profile->address->first()->getValue() : [],
        'shipping_method_id' => $default_rate->getShippingMethodId(),
        'order_id' => $shipment->getOrderId(),
        'shipment_id' => $shipment->id(),
      ]
    );

    // Workaround for massageFormValues() not getting $element.
    $element['array_parents'] = [
      '#type' => 'value',
      '#value' => $element['#field_parents'],
    ];

    return $element;
  }

}
