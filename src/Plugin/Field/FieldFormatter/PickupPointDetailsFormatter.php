<?php

declare(strict_types=1);

namespace Drupal\commerce_dpd_fr\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Plugin implementation of the 'commerce_dpd_fr_pickup_point_details' formatter.
 *
 * @FieldFormatter(
 *   id = "commerce_dpd_fr_pickup_point_details",
 *   label = @Translation("Pickup point details"),
 *   description = @Translation("Pickup point details"),
 *   field_types = {
 *     "string",
 *   },
 * )
 */
class PickupPointDetailsFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {

      // Gets the pickup point code from the field value.
      $pickup_point_code = $item->value;

      // Gets the shipment from the item
      $shipment = $item->getEntity();

      // Calls the API to retrieve the pickup point data.
      $pickup_point_details = $shipment
        ->getShippingMethod()
        ->getPlugin()
        ->getPickupPointDetails($pickup_point_code)
        [$pickup_point_code];

      // Builds the render array.
      $elements[$delta] = [
        '#theme' => 'commerce_dpd_fr_pickup_point_details',
        '#pickup_point_details' => [
          'pickup_point_id' => $pickup_point_code,
          'address' => [
            'name' => $pickup_point_details['name'],
            'address_line1' => $pickup_point_details['address1'],
            'postal_code' => $pickup_point_details['zipcode'],
            'locality' => $pickup_point_details['city'],
            'country' => 'France',
          ],
          'opening_hours' => $pickup_point_details['opening_hours'],
        ],
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // Checks if the field is either having the type commerce_shipment
    // or has "commerce_dpd_fr_pickup_point_id" as its name.
    $entity_type_id = $field_definition->getTargetEntityTypeId();
    $entity_name = $field_definition->getName();

    return $entity_type_id == 'commerce_shipment' && $entity_name == 'commerce_dpd_fr_pickup_point_id';
  }
}
