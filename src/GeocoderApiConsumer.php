<?php

declare(strict_types=1);

namespace Drupal\commerce_dpd_fr;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * The Google Map Geocoder API consumer.
 *
 * @package Drupal\commerce_dpd_fr\Services
 */
class GeocoderApiConsumer {

  private const API_URL = 'https://maps.googleapis.com/maps/api/geocode/json';

  /**
   * The Commerce DPD France logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Contructs a new GeocoderApiConsumer instance.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \GuzzleHttp\Client $http_client
   *   The http client.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory, Client $http_client) {
    $this->logger = $logger_factory->get('commerce_dpd_fr');
    $this->httpClient = $http_client;
  }

  /**
   * Gets latitude and longitude from an address.
   *
   * @param string $api_key
   *   The API key.
   * @param array $address
   *   The address.
   *
   * @return array|null
   *   The requested location on success,
   *   NULL if no API key stored or access denied.
   */
  public function getLocation(string $api_key, array $address): ?array {
    try {
      $response = $this->httpClient->request('GET', self::API_URL, [
        'query' => [
          'key' => $api_key,
          'address' => implode(' ', $address),
        ],
      ]);
      $data = Json::decode($response->getBody());
      if (isset($data['results'][0])) {
        return $data['results'][0]['geometry']['location'];
      }
    }
    catch (ClientException $e) {
      $this->logger->error($e->getMessage());
      return NULL;
    }

    return [];
  }

}
