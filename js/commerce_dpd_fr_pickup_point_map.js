/**
 * Creates the Commerce DPD France pickup point map.
 */

((Drupal, once) => {
  Drupal.commerceDpdFranceMap = {
    map: {},
    markers: [],
    infowindows: [],
    ppList: [],
    customerLocation: [],
    modulePath: '',
    pickupRadioName: '',
    init() {
      // Inits map, markers and infowindows
      const checked = document.querySelector('input[name="' + this.pickupRadioName + '"]:checked').value;
      this.constructMap(checked);
      this.addMarkers();

      // Construct customer address marker
      new google.maps.Marker({
        position: new google.maps.LatLng(this.customerLocation.lat, this.customerLocation.lng),
        map: this.map,
      });

      // Opens the first checked infowindow
      this.infowindows[checked].open(this.map, this.markers[checked]);

      // Handles click event on pickup point radio buttons
      const radio_buttons = document.querySelectorAll('input[name="' + this.pickupRadioName + '"]');
      radio_buttons.forEach(radio_button => {
        radio_button.addEventListener('click', () => {;
          Object.entries(this.infowindows).forEach(entry => {
            const [id, iw] = entry;
            iw.close();
          });
          const value = radio_button.value;
          this.infowindows[value].open(this.map, this.markers[value]);
        });
      });
    },
    constructMap(checked) {
      const map_wrapper = document.querySelector('.js-commerce-dpd-fr-pickup-point-map');
      this.map = new google.maps.Map(map_wrapper, {
        // Calls custom Google map style ID
        mapId: '6280f4115da1ec02',
        zoom: 14,
        center: new google.maps.LatLng(this.ppList[checked].lat, this.ppList[checked].lng),
        gestureHandling: 'greedy',
        disableDefaultUI: true,
      });
    },
    addMarkers() {
      Object.entries(this.ppList).forEach(entry => {
        const [id, pp] = entry;

        let marker = new google.maps.Marker({
          position: new google.maps.LatLng(pp.lat, pp.lng),
          map: this.map,
          icon: this.modulePath + '/img/pickup_locationmarker.png',
        });

        this.markers[id] = marker;
        let infowindow = this.infowindows[id] = this.infowindow(pp);

        // Event on markers
        google.maps.event.addListener(marker, 'click', () => {
          Object.entries(this.infowindows).forEach(entry => {
            const [id, iw] = entry;
            iw.close();
          });
          let radio = document.querySelector('input[name="' + this.pickupRadioName + '"][value="' + id + '"]');
          radio.click();
          infowindow.open(this.map, marker);
        });
      });
    },
    infowindow(pp) {
      const class_name = 'commerce-dpd-fr-iw';

      let content_title = document.createElement('h6');
      content_title.innerHTML = pp.name;
      content_title.classList.add(class_name + '--title');

      let content_address = document.createElement('p');
      content_address.innerHTML = pp.address1 + '<br>' + pp.zipcode + ' ' + pp.city;
      content_address.classList.add(class_name + '--address');

      let content_distance = document.createElement('p');
      content_distance.innerHTML = (pp.distance / 1000).toFixed(3) + ' km';
      content_distance.classList.add(class_name + '--distance');

      let content_table = document.createElement('table');
      content_table.classList.add(class_name + '--hours');
      Object.entries(pp.opening_hours).forEach(entry => {
        const [day, hours] = entry;
        const tr = content_table.insertRow();
        const td_day = tr.insertCell();
        td_day.innerHTML = day + '&nbsp;:';
        const td_hour = tr.insertCell();
        td_hour.innerHTML = hours[0];
        if (hours[1]) {
          td_hour.innerHTML += ', ' + hours[1];
        }
      });

      const content = document.createElement('div');
      content.append(content_title, content_address, content_distance, content_table);
      return new google.maps.InfoWindow({
        content: content,
      });
    }
  }

  /**
   * Creates the Commerce DPD France pickup point map.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   */
  Drupal.behaviors.commerceDpdPickupPointMap = {
    attach() {
      // Check if google variable is defined to avoid errors when google maps is not loaded.
      if (typeof google === 'undefined') {
        return;
      }
      const inputs = once('commerce-dpd-fr-pickup-point-map', '.js-commerce-dpd-fr-pickup-point-map');
      inputs.forEach(() => {
        // We only need to keep the last element of drupalSettings.commerce_dpd_fr_pickup_point_map.pickup_point_list
        // because the other ones are previous ppList that was merged in drupalSettings during ajax callback.
        // @todo Fix the merging drupalSettings array and send proper data to js
        Drupal.commerceDpdFranceMap.ppList = drupalSettings.commerce_dpd_fr_pickup_point_map.pickup_point_list.pop();
        Drupal.commerceDpdFranceMap.customerLocation = drupalSettings.commerce_dpd_fr_pickup_point_map.customer_location;
        Drupal.commerceDpdFranceMap.modulePath = drupalSettings.commerce_dpd_fr_pickup_point_map.module_path;
        Drupal.commerceDpdFranceMap.pickupRadioName = drupalSettings.commerce_dpd_fr_pickup_point_map.radio_path;
        Drupal.commerceDpdFranceMap.init();
      });
    }
  }
})(Drupal, once);

// We do not know the context at this point so we cannot call it in the attach() method.
window.initMap = Drupal.behaviors.commerceDpdPickupPointMap.attach;
